<!DOCTYPE html>
<html>
<head>
    <title>Вхід</title>
</head>
<body>
<h2>Форма входу</h2>

<?php
// Перевірити, чи була відправлена форма
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Отримати дані з форми
    $login = $_POST['login'];
    $password = $_POST['password'];

    // Функція для перевірки логіна та пароля
    function check_credentials($login, $password) {
        $usersFile = 'accounts.txt';
        $file = fopen($usersFile, 'r');
        if ($file) {
            while (($line = fgets($file)) !== false) {
                $storedCredentials = explode(' ', trim($line));
                $storedLogin = $storedCredentials[0];
                $storedPassword = $storedCredentials[1];
                if ($login == $storedLogin && $password == $storedPassword) {
                    fclose($file);
                    return true;
                }
            }
            fclose($file);
        }
        return false;
    }

    // Перевірити логін та пароль
    if (check_credentials($login, $password)) {
        // Вивести логін на екран
        echo "<p>Логін: $login</p>";

        // Зберегти кількість спроб входу у файл
        $attemptsFile = "$login.txt";
        if (file_exists($attemptsFile)) {
            $attempts = (int)file_get_contents($attemptsFile);
        } else {
            $attempts = 0;
        }

        $attempts++;

        file_put_contents($attemptsFile, $attempts);
    } else {
        // Невірний логін або пароль
        echo "<p>Невірний логін або пароль</p>";
    }
}
?>

<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
    <label for="login">Логін:</label>
    <input type="text" id="login" name="login" required><br><br>
    <label for="password">Пароль:</label>
    <input type="password" id="password" name="password" required><br><br>
    <input type="submit" value="Увійти">
</form>
</body>
</html>
