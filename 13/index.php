<?php include 'read.php'; ?>
<?php include 'filter.php'; ?>

<!DOCTYPE html>
<html>
<head>
    <title>User List</title>
</head>
<body>
<h1>User List</h1>

<?php foreach ($filteredUsers as $user): ?>
    <h3>Name: <?php echo $user['name']; ?></h3>
    <p>Login: <?php echo $user['login']; ?></p>
    <p>Email: <?php echo $user['email']; ?></p>
    <p>Language: <?php echo $user['language']; ?></p>
    <hr>
<?php endforeach; ?>
</body>
</html>
