<?php
$users = [];

$file = fopen('users.txt', 'r');
while (($line = fgets($file)) !== false) {
    $userData = explode('|', $line);
    $user = [
        'name' => $userData[0],
        'login' => $userData[1],
        'password' => $userData[2],
        'email' => $userData[3],
        'language' => $userData[4]
    ];
    $users[] = $user;
}

fclose($file);
?>
