<?php
// Отримати дані товарів з файлу
$products = [];
$file = fopen('tovar.txt', 'r');
if ($file) {
    while (($line = fgets($file)) !== false) {
        $productData = explode(',', $line);
        $productName = $productData[0];
        $productPrice = $productData[1];
        $products[] = [
            'name' => $productName,
            'price' => $productPrice,
        ];
    }
    fclose($file);
}

// Додати вибрані товари у сесію
session_start();
if (isset($_POST['submit'])) {
    $selectedProducts = $_POST['products'];
    $_SESSION['selectedProducts'] = $selectedProducts;
    header('Location: koshik.php');
    exit();
} elseif (isset($_SESSION['selectedProducts'])) {
    $selectedProducts = $_SESSION['selectedProducts'];
} else {
    $selectedProducts = [];
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Список товарів</title>
</head>
<body>
<h1>Список товарів</h1>
<form method="POST" action="">
    <?php foreach ($products as $product): ?>
        <label>
            <input type="checkbox" name="products[]" value="<?php echo $product['name']; ?>" <?php echo in_array($product['name'], $selectedProducts) ? 'checked' : ''; ?>>
            <?php echo $product['name']; ?> - <?php echo $product['price']; ?> грн
        </label>
        <br>
    <?php endforeach; ?>
    <br>
    <input type="submit" name="submit" value="Замовити">
</form>
</body>
</html>
