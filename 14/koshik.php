<?php
session_start();
$selectedProducts = isset($_SESSION['selectedProducts']) ? $_SESSION['selectedProducts'] : [];
$products = [];
$file = fopen('tovar.txt', 'r');
if ($file) {
    while (($line = fgets($file)) !== false) {
        $productData = explode(',', $line);
        $productName = $productData[0];
        $productPrice = $productData[1];
        $products[$productName] = $productPrice;
    }
    fclose($file);
}
$totalPrice = 0;
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Кошик</title>
</head>
<body>
<h1>Кошик</h1>
<?php foreach ($selectedProducts as $selectedProduct): ?>
    <?php
    $price = $products[$selectedProduct];
    $totalPrice += $price;
    ?>
    <p><?php echo $selectedProduct; ?> - <?php echo $price; ?> грн</p>
<?php endforeach; ?>
<p>Загальна сума: <?php echo $totalPrice; ?> грн</p>
</body>
</html>
